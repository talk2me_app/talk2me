import { Talk2mePage } from './app.po';

describe('talk2me App', () => {
  let page: Talk2mePage;

  beforeEach(() => {
    page = new Talk2mePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
