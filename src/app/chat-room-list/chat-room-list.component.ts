import { Component, OnInit } from '@angular/core';
import { ChatRoomListService } from './chat-room-list.service';
import { Router } from "@angular/router";
import { SessionService } from "../session.service";

@Component({
  selector: 'app-chat-room-list',
  templateUrl: './chat-room-list.component.html',
  styleUrls: ['./chat-room-list.component.css']
})
export class ChatRoomListComponent implements OnInit {
  roomList;
  isLoading:Boolean=true;

  constructor(private _chatRoomListService:ChatRoomListService, private router: Router, private _sessionService:SessionService) { }

  ngOnInit() {
    this._chatRoomListService.getChatRoomList().subscribe(roomListData => {
      this.roomList=roomListData;
      this.isLoading=false;
    });
    
    if(!this._sessionService.authenticated){
      this.router.navigate(['/']);
    }
  }

}
