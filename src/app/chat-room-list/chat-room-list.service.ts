import { Injectable } from '@angular/core';
import 'rxjs/Rx';
import { AngularFire } from 'angularfire2';
import { User } from "../users/user/user";

@Injectable()
export class ChatRoomListService {

  roomsObservable;
  activeUsersObservable;
  userHandle;
  time = new Date().getTime();

  getChatRoomList(){
    return this.roomsObservable = this.af.database.list('/rooms').delay(400);
  }

  getRoom(roomKey){
    return this.roomsObservable = this.af.database.object('/rooms/'+roomKey);
  }

  getUsers(roomKey){
    return this.roomsObservable = this.af.database.object('/rooms/'+roomKey+'/users/');
  }
  addRoom(room){
    this.roomsObservable = this.af.database.list('/rooms');
    let newRoom =  this.roomsObservable.push(room);
    return newRoom.key;
  }

  removeUserFromRoom(room,user:User){
    this.af.database.object('/rooms/'+room.$key+'/users/'+user.uid+'/').remove();
    console.log(('/rooms/'+room.$key+'/users/'+user.uid));
    
  }

  addUserToRoom(room,user:User){
    this.userHandle = this.af.database.list('/rooms/'+room.$key+'/users/');
    this.userHandle.update(user.uid, user);
  }

  constructor(private af:AngularFire) { }

}
