import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2';

@Injectable()
export class MessagesService {
  messagesObservable;
  roomKey;

  getMassages(room){
    this.roomKey=room.$key;
    return this.messagesObservable = this.af.database.list('/rooms/'+this.roomKey+'/messages/');
  }
  addMessage(message,room){
    //let dateNow = this.af.database.ServerValue.TIMESTAMP;
    //console.log(dateNow);
    //message.timestamp=dateNow;
    this.getMassages(room);
    this.messagesObservable.push(message);
  }
  removeMessage(message){
    this.af.database.object('/rooms/'+this.roomKey+'/messages/').remove;
  }
  constructor(private af:AngularFire) { }

}
