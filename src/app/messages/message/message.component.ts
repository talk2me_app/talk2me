import { Component, OnInit } from '@angular/core';
import { ChatMessage } from "./chat-message";
//import {DatePipe} from '@angular/common/pipes';
import { Pipe, PipeTransform } from '@angular/core';


@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css'],
  inputs:['message']
})
export class MessageComponent implements OnInit {

  message:ChatMessage;
  constructor() { }

  ngOnInit() {
  }

}
