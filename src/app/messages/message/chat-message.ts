export class ChatMessage {
    content:string;
    timestamp:number;
    user:string;
    avatar:string;
    userId:string;
}
