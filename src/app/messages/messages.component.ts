import { Component, OnInit } from '@angular/core';
import {MessagesService} from './messages.service'
import { ChatRoom } from "../chat-room/chat-room";
import { ChatMessage } from "./message/chat-message";
import { SessionService } from "../session.service";


@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css'],
  inputs:['room']
})
export class MessagesComponent implements OnInit {
  messages;
  currentUid;
  message:ChatMessage;

  room:ChatRoom;
  constructor(private _messagesService:MessagesService, private _sessionService:SessionService) { }

  ngOnInit() {
    this._messagesService.getMassages(this.room).subscribe(messagesData => {
      this.messages = messagesData;
      console.log("messages loaded");
    });
    this.currentUid = this._sessionService.getCurrentUser().uid;

  }

}
