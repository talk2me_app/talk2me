import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {AngularFireModule} from 'angularfire2';
import {RouterModule, Routes} from '@angular/router';

import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ChatRoomComponent } from './chat-room/chat-room.component';
import { ChatRoomListComponent } from './chat-room-list/chat-room-list.component';

import {ChatRoomListService} from './chat-room-list/chat-room-list.service';
import {MessagesService} from './messages/messages.service';
import { MessagesComponent } from './messages/messages.component';
import { MessageComponent } from './messages/message/message.component';
import { CreateChatRoomComponent } from './create-chat-room/create-chat-room.component';
import { UserAuthComponent } from './user-auth/user-auth.component';
import { SessionService } from "./session.service";
import { LoginComponent } from './login/login.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './users/user/user.component';
import { UsersService } from "./users/users.service";
import { SpinnerComponent } from './shared/spinner/spinner.component';

export const firebaseConfig = {
    apiKey: "AIzaSyB8NhHMPzXHISWOh7a6FCCb-cbHFfYb89o",
    authDomain: "talk2meapp-fa25d.firebaseapp.com",
    databaseURL: "https://talk2meapp-fa25d.firebaseio.com",
    storageBucket: "talk2meapp-fa25d.appspot.com",
    messagingSenderId: "208325271714",
}

const appRoutes:Routes = [
  {path:'chat-room-list', component:ChatRoomListComponent},
  {path:'create-chat-room', component:CreateChatRoomComponent},
  {path: 'chat-room/:id', component: ChatRoomComponent},
  {path: 'user-auth', component: UserAuthComponent},
  {path: 'users', component: UsersComponent},
  {path:'', component:LoginComponent},
  {path: '**', component:PageNotFoundComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    ChatRoomComponent,
    ChatRoomListComponent,
    MessagesComponent,
    MessageComponent,
    CreateChatRoomComponent,
    UserAuthComponent,
    LoginComponent,
    UsersComponent,
    UserComponent,
    SpinnerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [ChatRoomListService,MessagesService,SessionService,UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
