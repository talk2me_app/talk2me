import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import { ChatRoomListService } from '../chat-room-list/chat-room-list.service';
import { ChatRoom } from "../chat-room/chat-room";
import { Router } from "@angular/router";
import { SessionService } from "../session.service";


@Component({
  selector: 'app-create-chat-room',
  templateUrl: './create-chat-room.component.html',
  styleUrls: ['./create-chat-room.component.css']
})
export class CreateChatRoomComponent implements OnInit {

  userName:string = this._sessionService.getCurrentUser().name;
  userId:string = this._sessionService.getCurrentUser().uid;
  room:ChatRoom = {room_title:'',admin: this.userName, adminId: this.userId};
  noName:boolean=false;
  formData:NgForm;

  onSubmit(form:NgForm){
    this.userName = this._sessionService.getCurrentUser().name;
    this.userId = this._sessionService.getCurrentUser().uid;
    console.log(this.room);
    let newRoomKey:String = this._chatRoomListService.addRoom(this.room);
    this.room = {room_title:'',admin:'',adminId:''};
    this.router.navigate(['chat-room/'+newRoomKey]);
    console.log(newRoomKey);
  }

  constructor(private _chatRoomListService:ChatRoomListService, private router: Router, private _sessionService:SessionService) {
   }

  ngOnInit() {
    this.userName = this._sessionService.getCurrentUser().name;
    this.userId = this._sessionService.getCurrentUser().uid;
    console.log("username has been set to:");
    console.log(this.userName);
  }

}
