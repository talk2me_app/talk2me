import { Component, OnInit } from '@angular/core';
import {ChatRoom} from './chat-room';
import { NgForm } from '@angular/forms';
import { MessagesService } from "../messages/messages.service";
import { ChatMessage } from "../messages/message/chat-message";
import { ActivatedRoute, Router } from "@angular/router";
import { ChatRoomListService } from "../chat-room-list/chat-room-list.service";
import { SessionService } from "../session.service";
import { User } from "../users/user/user";

@Component({
  selector: 'app-chat-room',
  templateUrl: './chat-room.component.html',
  styleUrls: ['./chat-room.component.css'],
  inputs:['chatRoom']
})
export class ChatRoomComponent implements OnInit {
  chatRoom;
  formData:NgForm;
  time = new Date().getTime();
  userName = this._sessionService.getCurrentUser().name;
  userAvatar = this._sessionService.getCurrentUser().avatar;
  userId = this._sessionService.getCurrentUser().uid;
  message:ChatMessage = {content:'', timestamp:0, user:this.userName, avatar:this.userAvatar, userId:this.userId};
  roomKey;
  activeUsers;
  isAdmin:Boolean = false;

  submitForm(form:NgForm){
    console.log(this.time);
    this.message.timestamp=this.time;
    this._messagesService.addMessage(this.message,this.chatRoom);
    this.message = {content:'', timestamp:0, user:this.userName, avatar:this.userAvatar, userId:this.userId};
    this.scrolldown();
  }

  scrolldown(){
    setTimeout(function () {
      var objDiv = document.getElementById("messages");
      objDiv.scrollTop = objDiv.scrollHeight;
    }, 500);
    
  }

  addUser(user:User){
    this._chatRoomListService.addUserToRoom(this.chatRoom, user);
    this.message = {content:'Info: '+user.displayName+' has been added.', timestamp:this.time, user:'System', avatar:'', userId:''};
    this._messagesService.addMessage(this.message,this.chatRoom);
    this.message = {content:'', timestamp:0, user:this.userName, avatar:this.userAvatar, userId:this.userId};
  }

  removeUser(user){
    this._chatRoomListService.removeUserFromRoom(this.chatRoom, user);
    this.message = {content:'Info: '+user.displayName+' has been removed.', timestamp:this.time, user:'System', avatar:'', userId:''};
    this._messagesService.addMessage(this.message,this.chatRoom);
    this.message = {content:'', timestamp:0, user:this.userName, avatar:this.userAvatar, userId:this.userId};
  }

  constructor(private _messagesService:MessagesService, private _chatRoomListService:ChatRoomListService, route:ActivatedRoute, private router: Router, private _sessionService:SessionService) {
    this.roomKey = route.snapshot.params['id'];
     if (this.roomKey == null){
      router.navigate(['chat-room-list']);
    }
   }

  ngOnInit() {
    console.log("The room key that was passed is: "+this.roomKey);
    this._chatRoomListService.getRoom(this.roomKey).subscribe(roomData => {
      this.chatRoom=roomData;
      this.scrolldown();
    });

    this.activeUsers = this.chatRoom.users;
    this.scrolldown();
    console.log(this.chatRoom.adminId);
    if(this._sessionService.getCurrentUser().uid == this.chatRoom.adminId){
        this.isAdmin = true;
    }

  }

}
