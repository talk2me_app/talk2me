import { Injectable } from '@angular/core';

@Injectable()
export class SessionService {

  private currentUser: any;
  public authenticated:Boolean = false;

  setUser(user){
    this.currentUser = user;
    this.authenticated = true;
    console.log("session has saved this object:");
    console.log(user);
  }

  setAuthenticated(){
    this.authenticated = true;
    console.log("User authenticated");
  }

  deAuthenticate(){
    this.authenticated = false;
    console.log("User Logged-out");
  }

  getCurrentUser(){
    return this.currentUser;
  }

  

  constructor() { }

}
