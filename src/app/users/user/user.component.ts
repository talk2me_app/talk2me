import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { User } from "./user";
import 'rxjs/Rx';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  inputs: ['user','activeUsers']
})
export class UserComponent implements OnInit {
  user:User;
  selected:Boolean;
  activeUsers;
  @Output() addUsertoRoom = new EventEmitter<User>();
  @Output() removeUserfromRoom = new EventEmitter<User>();

  toggleSelected(){
    if(this.selected){
      this.addUsertoRoom.emit(this.user);
      
    }
    else{
      this.removeUserfromRoom.emit(this.user);
      console.log("emitted Remove from user.component");
    }
    console.log(this.selected);
  }

  constructor() { }

  ngOnInit() {
    // if (this.activeUsers.indexOf(this.user) >= 0){
    //   this.selected = true;
    // }
    // else{
    //   this.selected = false;
    // }

    //console.log(this.activeUsers.lastIndexOf(this.user));

    //let boo = (this.user instanceof this.activeUsers);

    // this.activeUsers.map(
    //   activeUser => {
    //     if (activeUser == this.user){
    //     this.selected = true;
    //   }
    //   }
    // );
    for (let key in this.activeUsers){
      if(key == this.user.uid){
        this.selected = true;
      }
    }

    console.log(this.activeUsers);
  }

}
