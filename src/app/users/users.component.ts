import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { UsersService } from "./users.service";
import { User } from "./user/user";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  inputs: ['activeUsers']
})
export class UsersComponent implements OnInit {

  users;
  isLoading:Boolean=true;
  activeUsers;
  @Output() addUsertoRoomFromUsers = new EventEmitter<User>();
  @Output() removeUserfromRoomFromUsers = new EventEmitter<User>();

  currentUser;

  changeCurrentUser(user){
    this.currentUser=user;
  }

  addUser(user){
    this.addUsertoRoomFromUsers.emit(user);
  }

  removeUser(user){
    this.removeUserfromRoomFromUsers.emit(user);
    console.log("emitted REMOVE from users.component");
  }

  constructor(private _usersService:UsersService) { }

  ngOnInit() {
    //this.users=this._usersService.getUsers();
    this._usersService.getUsers().subscribe(userData => {
      this.users=userData;
      this.isLoading=false;});
  }

}
