import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2';
import 'rxjs/Rx';

@Injectable()
export class UsersService {

  usersObservable;

  getUsers(){
    this.usersObservable = this.af.database.list('/users/').delay(400);
    return this.usersObservable;
  }

  constructor(private af:AngularFire) { }

}
