import { Component, OnInit } from '@angular/core';
import { AngularFire, AuthProviders, AuthMethods, FirebaseListObservable } from 'angularfire2';
import { SessionService } from "../session.service";
import { Router } from "@angular/router";


@Component({
  selector: 'app-user-auth',
  templateUrl: './user-auth.component.html',
  styleUrls: ['./user-auth.component.css']
})
export class UserAuthComponent implements OnInit {

  isAuth = false;
  authColor = 'warn';
  user = {};
  usersObservable;

  login(from: string) {
    this.af.auth.login({
      provider: AuthProviders.Google,
      method: AuthMethods.Popup
    });
  }
  logout() {
    this.af.auth.logout();
    this.user={};
    this._sessionService.deAuthenticate;
  }

  private _changeState(user: any = null) {
    if(user) {
      this.isAuth = true;
      this.authColor = 'primary';
      this.user = this._getUserInfo(user)
      this._sessionService.setUser(this.user);
      this._sessionService.setAuthenticated;
      this.router.navigate(['chat-room-list/']);
    }
    else {
      this.isAuth = false;
      this.authColor = 'warn';
      this.user = {};
    }
  }

  private _getUserInfo(user: any): any {
    if(!user) {
      return {};
    }
    let data = user.auth.providerData[0];

    //save users to firebase DB
    this.usersObservable = this.af.database.list('/users/');
    this.usersObservable.update(data.uid,data);
    //this.saveUserData(data);

    return {
      name: data.displayName,
      avatar: data.photoURL,
      email: data.email,
      uid: data.uid
    };
  }

//   saveUserData(data) {
//   this.af.database.ref('users/' + data.uid).set({
//     name: data.displayName,
//       avatar: data.photoURL,
//       email: data.email,
//       uid: data.uid
//   });
// }


  constructor(private af:AngularFire, private _sessionService:SessionService, private router: Router) {
    this.af.auth.subscribe(
      user => this._changeState(user),
      error => console.trace(error)
      );
   }


  ngOnInit() {
  }

}
